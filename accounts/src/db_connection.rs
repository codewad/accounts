use std::fmt::Display;

use async_trait::async_trait;

use tokio_postgres::ToStatement;
use tokio_postgres::row::RowIndex;
use tokio_postgres::types::ToSql;
use tokio_postgres::types::FromSql;

use super::Error;

#[async_trait]
pub trait Connection {
    type RowType;

    async fn execute<T>(&self, statement: &T, params: &[&(dyn ToSql + Sync)]) -> Result<u64, Error>
    where T: ?Sized + ToStatement + Sync;

    async fn query<T>(&self, statement: &T, params: &[&(dyn ToSql + Sync)]) -> Result<Vec<Self::RowType>, Error>
    where T: ?Sized + ToStatement + Sync;

    async fn query_one<T>(&self, statement: &T, params: &[&(dyn ToSql + Sync)]) -> Result<Self::RowType, Error>
    where T: ?Sized + ToStatement + Sync;
}

pub trait RawRow {
    fn get<'a, I, T>(&'a self, idx: I) -> T
    where I: RowIndex + Display,
          T: FromSql<'a>;
}
