use super::Error;
use crate::Connection;
use crate::RawRow;

pub struct PgAccountToRoleGateway<C: Connection + Sync + Send>
where <C as Connection>::RowType: RawRow
{
    client: C
}

impl<C: Connection + Sync + Send> PgAccountToRoleGateway<C>
where <C as Connection>::RowType: RawRow
{
    pub fn new(client: C) -> Self {
        Self {
            client
        }
    }

    pub async fn add(&mut self, account_id: i32, role_id: i32) -> Result<(), Error> {
        match self.client.execute("INSERT INTO account_to_role (account_id, role_id) VALUES ($1,$2)", &[&account_id, &role_id]).await {
            Ok(_) => Ok(()),
            Err(e) => {
                if e.matches_db_constraint("account_to_role_account_id_role_id_key") {
                    // The unique constraint was triggered
                    Err(Error::RecordAlreadyExists)
                } else {
                    Err(e)
                }
            }
        }
    }

    pub async fn get_roles_for_account(&mut self, account_id: i32) -> Result<Vec<i32>, ()> {
        match self.client.query("SELECT role_id FROM account_to_role WHERE account_id = $1", &[&account_id]).await {
            Ok(rows) => Ok(rows.iter().map(|row| row.get("role_id")).collect()),
            Err(_) => Err(())
        }
    }
}
