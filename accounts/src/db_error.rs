#[derive(Debug)]
pub enum Error {
    DbError(tokio_postgres::Error),
    RecordMissing,
    RecordAlreadyExists,
    LoginFailed
}

impl Error {
    pub fn matches_db_constraint(&self, constraint_name: &str) -> bool {
        if let Error::DbError(e) = self {
            if let Some(constraint) = e.as_db_error().unwrap().constraint() {
                if constraint == constraint_name {
                    true
                } else {
                    false
                }
            } else {
                false
            }
        } else {
            false
        }
    }
}
