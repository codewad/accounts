#[derive(Debug)]
pub enum Directive {
    Allow,
    Deny
}

impl From<bool> for Directive {
    fn from(o: bool) -> Self {
        if o {
            Directive::Allow
        } else {
            Directive::Deny
        }
    }
}

#[derive(Debug, Hash)]
pub struct Permission {
    id: i32,
    name: String
}

impl Permission {
    pub fn new(id: i32, name: String) -> Self {
        Self {
            id,
            name
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }
}
