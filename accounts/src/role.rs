#[derive(Debug)]
pub struct Role {
    id: i32,
    name: String
}

impl Role {
    pub fn new(id: i32, name: String) -> Self {
        Self {
            id,
            name
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }
}
