use rand_core::OsRng;

use argon2::Argon2;
use argon2::Params;
use argon2::Version;
use argon2::Algorithm;
use argon2::password_hash::PasswordHasher;
use argon2::password_hash::PasswordHash;
use argon2::password_hash::SaltString;
use argon2::password_hash::PasswordVerifier;

use super::SimplePasswordHasher;
use super::SimplePasswordVerifier;

pub struct ArgonContext<'a> {
    argon: Argon2<'a>
}

impl<'a> ArgonContext<'a> {
    pub fn new() -> Self {
        let params = Params::new(
            Params::DEFAULT_M_COST,
            Params::DEFAULT_T_COST,
            Params::DEFAULT_P_COST,
            Some(Params::DEFAULT_OUTPUT_LEN)
        ).unwrap();

        Self {
            argon: Argon2::new(Algorithm::Argon2id, Version::V0x13, params)
        }
    }
}

impl SimplePasswordHasher for ArgonContext<'_> {
    fn hash_password(&self, password: &[u8]) -> String {
        let salt = SaltString::generate(&mut OsRng);

        self.argon.hash_password(password, &salt).unwrap().to_string()
    }
}

impl SimplePasswordVerifier for ArgonContext<'_> {
    fn verify_password(&self, password: &[u8], hash: &str) -> bool {
        let parsed_hash = PasswordHash::new(&hash).unwrap();

        self.argon.verify_password(password, &parsed_hash).is_ok()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_argon_context() {
        let password = b"12345";

        let hash = ArgonContext::new().hash_password(password);

        assert!(ArgonContext::new().verify_password(password, &hash));
    }
}
