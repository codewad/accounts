use async_trait::async_trait;

mod db_connection;
pub use db_connection::Connection;
pub use db_connection::RawRow;

mod argon_context;
pub use argon_context::ArgonContext;

mod pg_account_repository;
pub use pg_account_repository::PgAccountRepository;

mod pg_permission_repository;
pub use pg_permission_repository::PgPermissionRepository;

mod pg_role_repository;
pub use pg_role_repository::PgRoleRepository;

mod pg_role_to_permission_gateway;
pub use pg_role_to_permission_gateway::PgRoleToPermissionGateway;

mod pg_account_to_role_gateway;
pub use pg_account_to_role_gateway::PgAccountToRoleGateway;

mod account;
pub use account::Account;

mod role;
pub use role::Role;

mod db_error;
pub use db_error::Error;

mod permission;
pub use permission::Permission;
pub use permission::Directive;

pub trait SimplePasswordHasher {
    fn hash_password(&self, password: &[u8]) -> String;
}

pub trait SimplePasswordVerifier {
    fn verify_password(&self, password: &[u8], hash: &str) -> bool;
}

#[async_trait]
pub trait AccountRepository {
    async fn create_account(&mut self, username: &str, password_hash: &str) -> Result<(), Error>;
    async fn find_account_by_username(&mut self, username: &str) -> Result<i32, Error>;
    async fn get_account_hashed_password(&mut self, username: &str) -> Option<String>;
}

#[async_trait]
pub trait Repository {
    async fn create(&self, name: &str) -> Result<(), Error>;
    async fn find_by_name(&self, name: &str) -> Result<i32, Error>;
}

pub struct AccountManager<R, V, C>
where R: AccountRepository,
      V: SimplePasswordVerifier + SimplePasswordHasher,
      C: Connection + Sync + Send,
      <C as Connection>::RowType: RawRow
{
    account_repository: R,
    permission_repository: PgPermissionRepository<C>,
    role_repository: PgRoleRepository<C>,
    password_hasher: V,
    role_to_permission_gateway: PgRoleToPermissionGateway<C>,
    account_to_role_gateway: PgAccountToRoleGateway<C>
}


impl<R, V, C> AccountManager<R, V, C>
where R: AccountRepository,
      V: SimplePasswordVerifier + SimplePasswordHasher,
      C: Connection + Sync + Send,
      <C as Connection>::RowType: RawRow
{
    pub fn new(
        account_repository: R,
        permission_repository: PgPermissionRepository<C>,
        role_repository: PgRoleRepository<C>,
        password_hasher: V,
        role_to_permission_gateway: PgRoleToPermissionGateway<C>,
        account_to_role_gateway: PgAccountToRoleGateway<C>
    ) -> Self {
        Self {
            account_repository,
            permission_repository,
            role_repository,
            password_hasher,
            role_to_permission_gateway,
            account_to_role_gateway,
        }
    }

    pub async fn login(&mut self, username: &str, password: &str) -> Result<Account, Error> {
        if let Some(hash) = self.account_repository.get_account_hashed_password(username).await {
            if self.password_hasher.verify_password(password.as_bytes(), &hash) {
                return match self.account_repository.find_account_by_username(username).await {
                    Ok(account_id) => Ok(Account::new(account_id, username.to_string())),
                    Err(_) => Err(Error::LoginFailed)
                };
            }
        }

        Err(Error::LoginFailed)
    }

    pub async fn create_account(&mut self, username: &str, password: &str) -> Result<(), Error> {
        if let Ok(_) = self.account_repository.find_account_by_username(username).await {
            return Err(Error::RecordMissing);
        }

        let password_hash = self.password_hasher.hash_password(password.as_bytes());
        self.account_repository.create_account(username, &password_hash).await
    }

    pub async fn create_permission(&mut self, name: &str) -> Result<(), Error> {
        if let Ok(_) = self.permission_repository.find_by_name(name).await {
            return Err(Error::RecordAlreadyExists);
        }

        self.permission_repository.create(name).await
    }

    pub async fn create_role(&mut self, name: &str) -> Result<(), Error> {
        match self.role_repository.find_by_name(name).await {
            Ok(_) => Err(Error::RecordAlreadyExists),
            Err(e) => {
                println!("Error: {:?}", e);
                self.role_repository.create(name).await
            }
        }

    }

    pub async fn add_role_permission(&mut self, role_name: &str, permission_name: &str, allow: bool) -> Result<(), Error> {
        let role_id = self.role_repository.find_by_name(role_name).await?;
        let permission_id = self.permission_repository.find_by_name(permission_name).await?;

        self.role_to_permission_gateway.add(role_id, permission_id, allow).await
    }

    pub async fn add_account_role(&mut self, account_name: &str, role_name: &str) -> Result<(), Error> {
        let account_id = self.account_repository.find_account_by_username(account_name).await?;
        let role_id = self.role_repository.find_by_name(role_name).await?;

        self.account_to_role_gateway.add(account_id, role_id).await
    }

    pub async fn does_account_have_permission(&mut self, account_name: &str, permission_name: &str) -> bool {
        if let Ok(permission_id) = self.permission_repository.find_by_name(permission_name).await {
            match self.account_repository.find_account_by_username(account_name).await {
                Ok(account_id) => {
                    let roles = self.account_to_role_gateway.get_roles_for_account(account_id).await.unwrap();
                    let permissions = self.role_to_permission_gateway.find_permission_in_roles(permission_id, &roles).await.unwrap();

                    if permissions.iter().any(|p| *p == false) {
                        false
                    } else if permissions.iter().any(|p| *p == true) {
                        true
                    } else {
                        false
                    }
                },
                Err(_) => false
            }
        } else {
            false
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::HashMap;

    struct DummyPasswordHasher;
    impl DummyPasswordHasher {
        fn hash(&self, s: &[u8]) -> String {
            format!("hashed {:?}", s).to_string()
        }
    }
    impl SimplePasswordVerifier for DummyPasswordHasher {
        fn verify_password(&self, password: &[u8], hash: &str) -> bool {
            self.hash(password) == hash.to_string()
        }
    }
    impl SimplePasswordHasher for DummyPasswordHasher {
        fn hash_password(&self, password: &[u8]) -> String {
            self.hash(password)
        }
    }

    struct AccountRepo {
        accounts: HashMap<String, (i32, String)>,
    }

    // impl AccountRepository for AccountRepo {
    //     fn create_account(&mut self, username: &str, password_hash: &str) -> Result<(), Error> {
    //         self.accounts.insert(username.to_string(), (10, password_hash.to_string()));
    //         Ok(())
    //     }

    //     fn get_account_hashed_password(&mut self, username: &str) -> Option<String> {
    //         Some(self.accounts.get(username)?.1.clone())
    //     }

    //     fn find_account_by_username(&mut self, username: &str) -> Result<i32, Error> {
    //         match self.accounts.get(username) {
    //             Some(account) => Ok(account.0),
    //             _ => Err(Error::RecordMissing)
    //         }
    //     }
    // }

    // use tokio_postgres::ToStatement;
    // use tokio_postgres::types::ToSql;
    // use db_connection::Connection;
    // use db_connection::ConnectionWrapper;

    // struct DummyRow;

    // #[async_trait]
    // impl Connection for ConnectionWrapper<()> {
    //     type RowType = DummyRow;

    //     async fn execute<T>(&self, statement: &T, params: &[&(dyn ToSql + Sync)]) -> Result<u64, tokio_postgres::Error>
    //     where T: ?Sized + ToStatement + Sync
    //     {
    //         Ok(1)
    //     }

    //     async fn query<T>(&self, statement: &T, params: &[&(dyn ToSql + Sync)]) -> Result<Vec<Self::RowType>, Error>
    //     where T: ?Sized + ToStatement + Sync
    //     {
    //         Ok(vec![])
    //     }

    //     async fn query_one<T>(&self, statement: &T, params: &[&(dyn ToSql + Sync)]) -> Result<Self::RowType, Error>
    //     where T: ?Sized + ToStatement + Sync
    //     {
    //         Ok(Row::new())
    //     }
    // }

    // #[test]
    // fn test_account_login() {
    //     let repo = AccountRepo { accounts: HashMap::new() };
    //     let hasher = DummyPasswordHasher {};

    //     let mut manager = AccountManager::new(repo, hasher);
    //     manager.create_account("username", "password").unwrap();
    //     let account = manager.login("username", "password");

    //     assert!(account.is_ok());
    // }

    // #[test]
    // fn test_failed_account_login() {
    //     let repo = AccountRepo { accounts: HashMap::new() };
    //     let hasher = DummyPasswordHasher {};

    //     let mut manager = AccountManager::new(repo, hasher);

    //     assert!(manager.login("username", "password").is_err());
    // }

    // #[test]
    // fn test_create_account() {
    //     let repo = AccountRepo {
    //         accounts: HashMap::new(),
    //     };
    //     let hasher = DummyPasswordHasher {};

    //     let mut manager = AccountManager::new(repo, hasher);

    //     assert!(manager.account_repository.find_account_by_username("username").is_none());
    //     assert!(manager.create_account("username", "password").is_ok());
    //     assert!(manager.account_repository.find_account_by_username("username").is_some());
    // }

    // #[test]
    // fn test_create_account_fails_when_username_exists() {
    //     let repo = AccountRepo { accounts: HashMap::new() };
    //     let hasher = DummyPasswordHasher {};

    //     let mut manager = AccountManager::new(repo, hasher);
    //     manager.create_account("username", "password").unwrap();

    //     assert!(manager.create_account("username", "password").is_err());
    // }
}
