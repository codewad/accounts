use super::Error;
use crate::Connection;
use crate::RawRow;

pub struct PgRoleToPermissionGateway<C: Connection + Sync + Send> {
    client: C
}

impl<C: Connection + Sync + Send> PgRoleToPermissionGateway<C>
where <C as Connection>::RowType: RawRow
{
    pub fn new(client: C) -> Self {
        Self {
            client
        }
    }

    pub async fn add(&mut self, role_id: i32, permission_id: i32, allow: bool) -> Result<(), Error> {
        match self.client.execute("INSERT INTO role_to_permission (role_id, permission_id, allow) VALUES ($1,$2,$3)", &[&role_id, &permission_id, &allow]).await {
            Ok(_) => Ok(()),
            Err(e) => {
                if e.matches_db_constraint("role_to_permission_role_id_permission_id_key") {
                    // The unique constraint was triggered
                    Err(Error::RecordAlreadyExists)
                } else {
                    Err(e)
                }
            }
        }
    }

    pub async fn get_permissions_for_role(&mut self, role_id: i32) -> Result<Vec<(i32, bool)>, Error> {
        match self.client.query("SELECT permission_id, allow FROM role_to_permission WHERE role_id = $1", &[&role_id]).await {
            Ok(rows) => Ok(rows.iter().map(|row| (row.get("permission_id"), row.get("allow"))).collect()),
            Err(e) => Err(e)
        }
    }

    pub async fn get_permissions_for_roles(&mut self, role_ids: &Vec<i32>) -> Result<Vec<(i32, bool)>, Error> {
        match self.client.query("SELECT permission_id, allow FROM role_to_permission WHERE role_id in $1", &[role_ids]).await {
            Ok(rows) => Ok(rows.iter().map(|row| (row.get("permission_id"), row.get("allow"))).collect()),
            Err(e) => Err(e)
        }
    }

    pub async fn find_permission_in_roles(&mut self, permission_id: i32, role_ids: &Vec<i32>) -> Result<Vec<bool>, Error> {
        match self.client.query("SELECT allow FROM role_to_permission WHERE permission_id = $2 AND role_id = ANY($1)", &[role_ids, &permission_id]).await {
            Ok(rows) => Ok(rows.iter().map(|row| row.get("allow")).collect()),
            Err(e) => Err(e)
        }
    }
}
