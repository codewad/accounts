use async_trait::async_trait;

use super::Permission;
use super::Repository;
use super::Error;

use crate::Connection;
use crate::RawRow;

pub struct PgPermissionRepository<C: Connection + Sync + Send> {
    client: C
}

impl<C: Connection + Sync + Send> PgPermissionRepository<C>
where <C as Connection>::RowType: RawRow
{
    pub fn new(client: C) -> Self {
        Self {
            client
        }
    }

    pub async fn find_many_by_id(&mut self, ids: &Vec<i32>) -> Vec<Permission> {
        match self.client.query("SELECT id, name FROM permissions WHERE id IN $1", &[ids]).await {
            Ok(rows) => rows.iter().map(|row| Permission::new(row.get("id"), row.get("name"))).collect(),
            Err(_) => Vec::new()
        }
    }
}

#[async_trait]
impl<C: Connection + Sync + Send> Repository for PgPermissionRepository<C>
where <C as Connection>::RowType: RawRow
{
    async fn create(&self, name: &str) -> Result<(), Error> {
        match self.client.execute("INSERT INTO permissions (name) VALUES ($1)", &[&name.to_string()]).await {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }

    async fn find_by_name(&self, name: &str) -> Result<i32, Error> {
        match self.client.query_one("select id from permissions WHERE name = $1", &[&name.to_string()]).await {
            Ok(row) => Ok(row.get("id")),
            Err(e) => Err(e)
        }
    }
}
