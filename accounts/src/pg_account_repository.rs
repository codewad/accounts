use async_trait::async_trait;

use crate::Connection;
use crate::RawRow;

use super::AccountRepository;
use super::Error;

pub struct PgAccountRepository<C: Connection + Sync + Send> {
    client: C
}

impl<C: Connection + Sync + Send> PgAccountRepository<C> {
    pub fn new(client: C) -> Self {
        Self {
            client
        }
    }
}

#[async_trait]
impl<C: Connection + Sync + Send> AccountRepository for PgAccountRepository<C>
where <C as Connection>::RowType: RawRow
{
    async fn create_account(&mut self, username: &str, password_hash: &str) -> Result<(), Error> {
        match self.client.execute("INSERT INTO passwd (username, password) VALUES ($1, $2)", &[&username.to_string(), &password_hash.to_string()]).await {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }

    async fn find_account_by_username(&mut self, username: &str) -> Result<i32, Error> {
        match self.client.query_one("select id from passwd WHERE username = $1", &[&username.to_string()]).await {
            Ok(row) => Ok(row.get("id")),
            Err(e) => Err(e)
        }
    }

    async fn get_account_hashed_password(&mut self, username: &str) -> Option<String> {
        match self.client.query_one("select password from passwd WHERE username = $1", &[&username.to_string()]).await {
            Ok(row) => Some(row.get("password")),
            Err(_) => None
        }
    }
}
