use async_trait::async_trait;

//use tokio_postgres::Client;

use super::Repository;
use super::Error;

use crate::Connection;
use crate::RawRow;

pub struct PgRoleRepository<T: Connection + Sync + Send> {
    client: T
}

impl<T: Connection + Sync + Send> PgRoleRepository<T> {
    pub fn new(client: T) -> Self {
        Self {
            client
        }
    }
}

#[async_trait]
impl<T: Connection + Sync + Send> Repository for PgRoleRepository<T>
where <T as Connection>::RowType: RawRow
{
    async fn create(&self, name: &str) -> Result<(), Error> {
        match self.client.execute("INSERT INTO roles (name) VALUES ($1)", &[&name.to_string()]).await {
            Ok(_) => Ok(()),
            Err(e) => Err(e)
        }
    }

    async fn find_by_name(&self, name: &str) -> Result<i32, Error> {
        match self.client.query_one("select id from roles WHERE name = $1", &[&name.to_string()]).await {
            Ok(row) => Ok(row.get("id")),
            Err(e) => Err(e)
        }
    }
}
