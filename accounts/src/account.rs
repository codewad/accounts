#[derive(Debug)]
pub struct Account {
    id: i32,
    username: String
}

impl Account {
    pub fn new(id: i32, username: String) -> Self {
        Self {
            id,
            username
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn get_name(&self) -> &str {
        &self.username
    }
}
