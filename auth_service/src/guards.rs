use rocket::http::Status;
use rocket::request::Outcome;
use rocket::request::FromRequest;
use rocket::request::Request;
use rocket::State;

use deadpool_postgres::Pool;

use crate::db::ConnectionWrapper;

use accounts::AccountManager as InnerAccountManager;
use accounts::PgAccountRepository;
use accounts::PgPermissionRepository;
use accounts::PgRoleRepository;
use accounts::ArgonContext;
use accounts::PgRoleToPermissionGateway;
use accounts::PgAccountToRoleGateway;

type InnerAM<'a> = InnerAccountManager<PgAccountRepository<ConnectionWrapper>, ArgonContext<'a>, ConnectionWrapper>;

pub struct AccountManager<'a>(InnerAM<'a>);

impl<'a> std::ops::Deref for AccountManager<'a> {
    type Target = InnerAM<'a>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'a> std::ops::DerefMut for AccountManager<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[derive(Debug)]
pub enum DbConnectionError {
    Oops
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for ConnectionWrapper {
    type Error = DbConnectionError;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match request.guard::<&State<Pool>>().await {
            Outcome::Success(pool) => match pool.get().await {
                Ok(c) => Outcome::Success(Self::new(c)),
                Err(_) => Outcome::Failure((Status::InternalServerError, DbConnectionError::Oops))
            },
            Outcome::Failure(_) => Outcome::Failure((Status::InternalServerError, DbConnectionError::Oops)),
            Outcome::Forward(f) => Outcome::Forward(f)
        }
    }
}

#[rocket::async_trait]
impl<'a, 'r> FromRequest<'r> for AccountManager<'a> {
    type Error = DbConnectionError;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match request.guard::<ConnectionWrapper>().await {
            Outcome::Success(client) => {
                Outcome::Success(AccountManager(InnerAM::new(
                    PgAccountRepository::new(client.clone()),
                    PgPermissionRepository::new(client.clone()),
                    PgRoleRepository::new(client.clone()),
                    ArgonContext::new(),
                    PgRoleToPermissionGateway::new(client.clone()),
                    PgAccountToRoleGateway::new(client.clone()),
                )))
            },
            Outcome::Failure(_) => Outcome::Failure((Status::InternalServerError, DbConnectionError::Oops)),
            Outcome::Forward(f) => Outcome::Forward(f)
        }
    }
}
