use std::sync::Arc;
use std::fmt::Display;
use tokio::sync::Mutex;

use tokio_postgres::NoTls;
use tokio_postgres::ToStatement;
use tokio_postgres::types::ToSql;
use tokio_postgres::types::FromSql;
use tokio_postgres::row::RowIndex;
use tokio_postgres::Row;

use deadpool_postgres::ManagerConfig;
use deadpool_postgres::Manager;
use deadpool_postgres::Pool;
use deadpool_postgres::RecyclingMethod;
use deadpool_postgres::Object;

use accounts::Error;
use accounts::Connection;
use accounts::RawRow;

pub fn create_db_pool() -> Pool {
    let mut pg_config = tokio_postgres::Config::new();
    pg_config
        .host("192.168.122.81")
        .user("account_manager")
        .dbname("passwd");

    let manager_config = ManagerConfig {
        recycling_method: RecyclingMethod::Fast
    };

    let manager = Manager::from_config(pg_config, NoTls, manager_config);
    Pool::builder(manager).max_size(8).build().unwrap()
}


pub struct RowWrapper {
    row: Row
}

impl RowWrapper {
    fn new(row: Row) -> Self {
        Self {
            row
        }
    }
}

impl RawRow for RowWrapper {
    fn get<'a, I, T>(&'a self, idx: I) -> T
    where I: RowIndex + Display,
          T: FromSql<'a>
    {
        self.row.get(idx)
    }
}

#[derive(Clone)]
pub struct ConnectionWrapper {
    connection: Arc<Mutex<Object>>
}

impl ConnectionWrapper {
    pub fn new(connection: Object) -> Self {
        Self {
            connection: Arc::new(Mutex::new(connection))
        }
    }
}


#[async_trait]
impl Connection for ConnectionWrapper
{
    type RowType = RowWrapper;

    async fn execute<T>(&self, statement: &T, params: &[&(dyn ToSql + Sync)]) -> Result<u64, Error>
    where T: ?Sized + ToStatement + Sync
    {
        match self.connection.lock().await.execute(statement, params).await {
            Ok(i) => Ok(i),
            Err(e) => Err(Error::DbError(e))
        }
    }

    async fn query<T>(&self, statement: &T, params: &[&(dyn ToSql + Sync)]) -> Result<Vec<Self::RowType>, Error>
    where T: ?Sized + ToStatement + Sync
    {
        let raw_rows = match self.connection.lock().await.query(statement, params).await {
            Ok(x) => Ok(x),
            Err(e) => Err(Error::DbError(e))
        }?;

        Ok(raw_rows.into_iter().map(RowWrapper::new).collect())
    }

    async fn query_one<T>(&self, statement: &T, params: &[&(dyn ToSql + Sync)]) -> Result<Self::RowType, Error>
    where T: ?Sized + ToStatement + Sync
    {
        let raw_row = match self.connection.lock().await.query_one(statement, params).await {
            Ok(x) => Ok(x),
            Err(e) => Err(Error::DbError(e))
        }?;

        Ok(RowWrapper::new(raw_row))
    }
}
