#[macro_use]
extern crate rocket;

mod guards;
use guards::AccountManager;

mod db;

#[get("/example")]
async fn example(mut manager: AccountManager<'_>) -> String {
    format!("
{:?}
{:?}
{:?}
{:?}
{:?}
{:?}
",
            manager.create_account("username", "password").await,
            manager.create_role("user").await,
            manager.create_permission("account.login").await,
            manager.add_account_role("username", "user").await,
            manager.add_role_permission("user", "account.login", true).await,

            //println!("{:?}", manager.login("username", "password"));
            manager.does_account_have_permission("username", "account.login").await
    ).to_string()
}

#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    let _rocket = rocket::build()
        .manage(db::create_db_pool())
        .mount("/", routes![example])
        .launch()
        .await?;

    Ok(())
}
